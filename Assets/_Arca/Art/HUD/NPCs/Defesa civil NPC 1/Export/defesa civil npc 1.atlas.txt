
defesa civil npc 1.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
P L
  rotate: false
  xy: 188, 28
  size: 20, 24
  orig: 20, 24
  offset: 0, 0
  index: -1
P R
  rotate: false
  xy: 596, 256
  size: 12, 16
  orig: 12, 16
  offset: 0, 0
  index: -1
arm L
  rotate: true
  xy: 498, 348
  size: 156, 312
  orig: 156, 312
  offset: 0, 0
  index: -1
arm R 1
  rotate: false
  xy: 2, 2
  size: 184, 148
  orig: 184, 148
  offset: 0, 0
  index: -1
arm R 2
  rotate: true
  xy: 498, 274
  size: 72, 148
  orig: 72, 148
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 152
  size: 256, 352
  orig: 256, 352
  offset: 0, 0
  index: -1
body_white mask
  rotate: true
  xy: 260, 178
  size: 36, 120
  orig: 36, 120
  offset: 0, 0
  index: -1
eyes 1
  rotate: false
  xy: 648, 302
  size: 92, 44
  orig: 92, 44
  offset: 0, 0
  index: -1
eyes 2
  rotate: false
  xy: 812, 374
  size: 96, 36
  orig: 96, 36
  offset: 0, 0
  index: -1
eyes 3
  rotate: true
  xy: 188, 54
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
eyes 4
  rotate: false
  xy: 498, 224
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
hand R
  rotate: false
  xy: 812, 412
  size: 104, 92
  orig: 104, 92
  offset: 0, 0
  index: -1
hand R finger 1
  rotate: true
  xy: 918, 460
  size: 44, 104
  orig: 44, 104
  offset: 0, 0
  index: -1
hand R finger 2
  rotate: false
  xy: 382, 178
  size: 32, 36
  orig: 32, 36
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 260, 216
  size: 236, 288
  orig: 236, 288
  offset: 0, 0
  index: -1
head neck
  rotate: false
  xy: 918, 398
  size: 72, 60
  orig: 72, 60
  offset: 0, 0
  index: -1
head s L
  rotate: false
  xy: 260, 152
  size: 44, 24
  orig: 44, 24
  offset: 0, 0
  index: -1
head s R
  rotate: false
  xy: 238, 138
  size: 20, 12
  orig: 20, 12
  offset: 0, 0
  index: -1
