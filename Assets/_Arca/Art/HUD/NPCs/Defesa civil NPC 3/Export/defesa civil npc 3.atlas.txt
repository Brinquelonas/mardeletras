
defesa civil npc 3.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
P L
  rotate: false
  xy: 886, 480
  size: 24, 24
  orig: 24, 24
  offset: 0, 0
  index: -1
P R
  rotate: false
  xy: 238, 134
  size: 12, 16
  orig: 12, 16
  offset: 0, 0
  index: -1
arm L
  rotate: false
  xy: 260, 192
  size: 156, 312
  orig: 156, 312
  offset: 0, 0
  index: -1
arm R 1
  rotate: false
  xy: 2, 2
  size: 184, 148
  orig: 184, 148
  offset: 0, 0
  index: -1
arm R 2
  rotate: false
  xy: 636, 356
  size: 72, 148
  orig: 72, 148
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 152
  size: 256, 352
  orig: 256, 352
  offset: 0, 0
  index: -1
body_white mask
  rotate: true
  xy: 260, 154
  size: 36, 120
  orig: 36, 120
  offset: 0, 0
  index: -1
eyes 1
  rotate: false
  xy: 710, 366
  size: 92, 44
  orig: 92, 44
  offset: 0, 0
  index: -1
eyes 2
  rotate: false
  xy: 524, 218
  size: 96, 36
  orig: 96, 36
  offset: 0, 0
  index: -1
eyes 3
  rotate: true
  xy: 188, 54
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
eyes 4
  rotate: true
  xy: 636, 258
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
hand R
  rotate: false
  xy: 710, 412
  size: 104, 92
  orig: 104, 92
  offset: 0, 0
  index: -1
hand R finger 1
  rotate: true
  xy: 418, 210
  size: 44, 104
  orig: 44, 104
  offset: 0, 0
  index: -1
hand R finger 2
  rotate: false
  xy: 382, 154
  size: 32, 36
  orig: 32, 36
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 418, 256
  size: 216, 248
  orig: 216, 248
  offset: 0, 0
  index: -1
head neck
  rotate: false
  xy: 816, 436
  size: 68, 68
  orig: 68, 68
  offset: 0, 0
  index: -1
head s L
  rotate: false
  xy: 188, 28
  size: 48, 24
  orig: 48, 24
  offset: 0, 0
  index: -1
head s R
  rotate: false
  xy: 418, 196
  size: 20, 12
  orig: 20, 12
  offset: 0, 0
  index: -1
