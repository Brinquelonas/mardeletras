
defesa civil npc 5.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
P L
  rotate: true
  xy: 526, 238
  size: 20, 24
  orig: 20, 24
  offset: 0, 0
  index: -1
P R
  rotate: false
  xy: 410, 170
  size: 12, 16
  orig: 12, 16
  offset: 0, 0
  index: -1
arm L
  rotate: true
  xy: 526, 354
  size: 156, 312
  orig: 156, 312
  offset: 0, 0
  index: -1
arm R 1
  rotate: false
  xy: 2, 8
  size: 184, 148
  orig: 184, 148
  offset: 0, 0
  index: -1
arm R 2
  rotate: true
  xy: 260, 164
  size: 72, 148
  orig: 72, 148
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 158
  size: 256, 352
  orig: 256, 352
  offset: 0, 0
  index: -1
body_white mask
  rotate: false
  xy: 840, 390
  size: 36, 120
  orig: 36, 120
  offset: 0, 0
  index: -1
eyes 1
  rotate: false
  xy: 632, 308
  size: 92, 44
  orig: 92, 44
  offset: 0, 0
  index: -1
eyes 2
  rotate: false
  xy: 878, 474
  size: 96, 36
  orig: 96, 36
  offset: 0, 0
  index: -1
eyes 3
  rotate: false
  xy: 188, 2
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
eyes 4
  rotate: false
  xy: 410, 188
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
hand R
  rotate: false
  xy: 526, 260
  size: 104, 92
  orig: 104, 92
  offset: 0, 0
  index: -1
hand R finger 1
  rotate: false
  xy: 188, 52
  size: 44, 104
  orig: 44, 104
  offset: 0, 0
  index: -1
hand R finger 2
  rotate: true
  xy: 840, 356
  size: 32, 36
  orig: 32, 36
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 260, 238
  size: 264, 272
  orig: 264, 272
  offset: 0, 0
  index: -1
head neck
  rotate: false
  xy: 878, 412
  size: 72, 60
  orig: 72, 60
  offset: 0, 0
  index: -1
head s L
  rotate: true
  xy: 234, 108
  size: 48, 24
  orig: 48, 24
  offset: 0, 0
  index: -1
head s R
  rotate: false
  xy: 260, 150
  size: 20, 12
  orig: 20, 12
  offset: 0, 0
  index: -1
