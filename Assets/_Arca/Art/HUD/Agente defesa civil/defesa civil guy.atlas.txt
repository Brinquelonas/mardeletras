
defesa civil guy.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
P L
  rotate: false
  xy: 260, 158
  size: 20, 24
  orig: 20, 24
  offset: 0, 0
  index: -1
P R
  rotate: false
  xy: 706, 290
  size: 12, 16
  orig: 12, 16
  offset: 0, 0
  index: -1
arm L
  rotate: true
  xy: 498, 354
  size: 156, 312
  orig: 156, 312
  offset: 0, 0
  index: -1
arm R 1
  rotate: false
  xy: 2, 8
  size: 184, 148
  orig: 184, 148
  offset: 0, 0
  index: -1
arm R 2
  rotate: true
  xy: 498, 280
  size: 72, 148
  orig: 72, 148
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 158
  size: 256, 352
  orig: 256, 352
  offset: 0, 0
  index: -1
body_white mask
  rotate: true
  xy: 260, 184
  size: 36, 120
  orig: 36, 120
  offset: 0, 0
  index: -1
eyes 1
  rotate: false
  xy: 648, 308
  size: 92, 44
  orig: 92, 44
  offset: 0, 0
  index: -1
eyes 2
  rotate: false
  xy: 812, 380
  size: 96, 36
  orig: 96, 36
  offset: 0, 0
  index: -1
eyes 3
  rotate: true
  xy: 188, 60
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
eyes 4
  rotate: false
  xy: 498, 230
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
hand R
  rotate: false
  xy: 812, 418
  size: 104, 92
  orig: 104, 92
  offset: 0, 0
  index: -1
hand R finger 1
  rotate: true
  xy: 918, 466
  size: 44, 104
  orig: 44, 104
  offset: 0, 0
  index: -1
hand R finger 2
  rotate: false
  xy: 440, 184
  size: 32, 36
  orig: 32, 36
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 260, 222
  size: 236, 288
  orig: 236, 288
  offset: 0, 0
  index: -1
head neck
  rotate: false
  xy: 918, 404
  size: 72, 60
  orig: 72, 60
  offset: 0, 0
  index: -1
head s L
  rotate: true
  xy: 992, 420
  size: 44, 24
  orig: 44, 24
  offset: 0, 0
  index: -1
head s R
  rotate: false
  xy: 238, 144
  size: 20, 12
  orig: 20, 12
  offset: 0, 0
  index: -1
mouth 1
  rotate: true
  xy: 234, 2
  size: 56, 36
  orig: 56, 36
  offset: 0, 0
  index: -1
mouth 2
  rotate: false
  xy: 382, 184
  size: 56, 36
  orig: 56, 36
  offset: 0, 0
  index: -1
mouth 3
  rotate: true
  xy: 188, 2
  size: 56, 44
  orig: 56, 44
  offset: 0, 0
  index: -1
mouth 4
  rotate: false
  xy: 812, 362
  size: 56, 16
  orig: 56, 16
  offset: 0, 0
  index: -1
mouth 5
  rotate: false
  xy: 742, 316
  size: 56, 36
  orig: 56, 36
  offset: 0, 0
  index: -1
mouth 6
  rotate: false
  xy: 596, 246
  size: 40, 32
  orig: 40, 32
  offset: 0, 0
  index: -1
mouth 7
  rotate: false
  xy: 648, 290
  size: 56, 16
  orig: 56, 16
  offset: 0, 0
  index: -1
