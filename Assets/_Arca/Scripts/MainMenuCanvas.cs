﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuCanvas : MonoBehaviour {
    
    public Button PlayGame;
    public Button ARGame;
    public Button SoundGame;
    public Button QuitButton;

    public PotaTween PlayGameTween;
    public PotaTween ArGameTween;
    public PotaTween SoundGameTween;
    public PotaTween Fader;

    public GameObject LoadingText;

    void OnEnable()
    {
        Invoke("Open", 0.1f);
    }

    void Start ()
    {
        PlayGame.onClick.AddListener(() =>
        {
            PlayGameTween.Reverse();
            SoundGameTween.Reverse();
            ArGameTween.Reverse();
            Fader.Reverse(() => SceneManager.LoadScene("LevelSelection"));
        });

        ARGame.onClick.AddListener(() =>
        {
            PlayGameTween.Reverse();
            ArGameTween.Reverse();
            SoundGameTween.Reverse();
            //Fader.Reverse(() => SceneManager.LoadScene("ArGame"));
            Fader.Reverse(() => LoadSceneAsync("ArGame"));
        });

        SoundGame.onClick.AddListener(() =>
        {
            PlayGameTween.Reverse();
            ArGameTween.Reverse();
            SoundGameTween.Reverse();
            Fader.Reverse(() => SceneManager.LoadScene("SoundGame"));
        });

        QuitButton.onClick.AddListener(() => 
        {
            PlayGameTween.Reverse();
            ArGameTween.Reverse();
            SoundGameTween.Reverse();
            Fader.Reverse(() => Application.Quit());
        });
    }
	
	void Update () {
		
	}

    void Open()
    {
        PlayGameTween.Play();
        ArGameTween.Play();
        SoundGameTween.Play();
        Fader.Play();
    }

    public void LoadSceneAsync(string sceneName)
    {
        LoadingText.SetActive(true);

        StartCoroutine(LoadingScene(sceneName));
    }

    private IEnumerator LoadingScene(string sceneName)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneName);

        while (!async.isDone)
            yield return null;
    }
}
