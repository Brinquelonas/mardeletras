﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour {

    private Button _button;
    private AudioSource _source;

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = PotaTween.Create(gameObject).SetScale(Vector3.one, Vector3.one * 1.2f).SetEaseEquation(Ease.Equation.InOutBack).SetDuration(0.5f);
            return _tween;
        }
    }

    void Start ()
    {
        _button = GetComponent<Button>();
        _source = GetComponent<AudioSource>();

        if(_source.clip != null)
        {
            _button.onClick.AddListener(() =>
            {
                _button.interactable = false;
                _source.Play();

                Invoke("EnableButton", _source.clip.length);
                Tween.Play();
            });
        }
    }
	
	void Update ()
    {
		
	}

    void EnableButton()
    {
        _button.interactable = true;
        Tween.Reverse();
    }
}
