﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyAR;

public class LetterSpawner : MonoBehaviour
{
    private static LetterSpawner _instance;
    public static LetterSpawner Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<LetterSpawner>();

            return _instance;
        }
    }

    public GameObject[] UpperLetters;
    public GameObject[] LowerLetters;

    private List<string> _upperLetters = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Á", "Ã", "Ç", "É", "Í", "Ó", "Õ", "Ú" };
    private List<string> _lowerLetters = new List<string> { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "á", "ã", "ç", "é", "í", "ó", "õ", "ú" };
    
    private WordSpawner[] _wordsSpawner;

    void Start ()
    {
        _wordsSpawner = GetComponentsInChildren<WordSpawner>(true);

        for (int i = 0; i < _wordsSpawner.Length; i++)
        {
            _wordsSpawner[i].Initialize();
        }
    }
	
	void Update () {
		
	}

    public GameObject SpawnUpperLetter(string letter)
    {
        int index = _upperLetters.IndexOf(letter);

        return Instantiate(UpperLetters[index]);
    }

    public GameObject SpawnLowerLetter(string letter)
    {
        int index = _lowerLetters.IndexOf(letter);

        return Instantiate(LowerLetters[index]);
    }
}
