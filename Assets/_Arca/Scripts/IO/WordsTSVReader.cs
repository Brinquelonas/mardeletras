﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordsTSVReader : MonoBehaviour {

    public TextAsset TSV;
    public List<string>[] Words;

    private static WordsTSVReader _instance;
    public static WordsTSVReader Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<WordsTSVReader>();

            return _instance;
        }
    }

    void Awake()
    {
        ReadTSV();
    }

    public void ReadTSV()
    {
        string[] lines = TSV.text.Split(System.Environment.NewLine[0]);
        Words = new List<string>[lines.Length];

        for (int i = 0; i < lines.Length; i++)
        {
            string[] contents = lines[i].Split("\t"[0]);
            Words[i] = new List<string>();

            for (int j = 0; j < contents.Length; j++)
            {
                if (!string.IsNullOrEmpty(contents[j].Trim()))
                    Words[i].Add(contents[j].Trim());
            }
        }
    }

    public List<string> GetWords(int index)
    {
        return Words[index];
    }
}
